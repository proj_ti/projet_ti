using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using APIContinent.Helpers;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using WebApi.Services;

namespace APIContinent
{
    public class Startup
    {
        private static readonly String PATH_ANGULAR_APP = "wwwroot/js/projetAngularAPP";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

// This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSpaStaticFiles(spa => spa.RootPath = PATH_ANGULAR_APP);

            services.AddCors();
            services.AddControllers();
          
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

                   // configure jwt authentication
                     var appSettings = appSettingsSection.Get<AppSettings>();
                     var key = Encoding.ASCII.GetBytes("THIS IS USED TO SIGN AND VERIFY JWT TOKENS");
                          services.AddAuthentication(x =>
                         {
                             x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                             x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                         })
                         .AddJwtBearer(x =>
                         {
                             x.RequireHttpsMetadata = false;
                             x.SaveToken = true;
                             x.TokenValidationParameters = new TokenValidationParameters
                             {
                                 ValidateIssuerSigningKey = true,
                                 IssuerSigningKey = new SymmetricSecurityKey(key),
                                 ValidateIssuer = false,
                                 ValidateAudience = false
                             };
                         });
         
                     // configure DI for application services
                     services.AddScoped<IUserService, UserService>(); 
        }



        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHttpsRedirection();
            }


            app.UseRouting();
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

            app.UseStaticFiles();
            app.UseSpaStaticFiles();
            app.UseSpa(Spa => Spa.Options.SourcePath = PATH_ANGULAR_APP);
        }
        
        
    }
}