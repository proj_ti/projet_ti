﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using APIContinent.DTO;
using APIContinent.Model;

namespace APIContinent.DAO
{
    public class paysDAO
    {
        private static readonly string TABLE_NAME = "Pays";

        public static string FIELD_ID = "Id_Pays";
        public static string FIELD_NOM = "Nom_Pays";
        public static string FIELD_DESC = "Description_pays";
        public static string FIELD_IDEX = "Id_Continent";
        
        private static readonly String REQ_QUERY = $"select * from {TABLE_NAME}";
        
        private static readonly string REQ_GET = REQ_QUERY + $" WHERE {FIELD_ID} = @{FIELD_ID}";

        private static readonly string REQ_POST = $"INSERT INTO {TABLE_NAME} ({FIELD_NOM}, {FIELD_IDEX},{FIELD_DESC}) " +
                                                  $"OUTPUT INSERTED.{FIELD_ID} " +
                                                  $"VALUES (@{FIELD_NOM},@{FIELD_IDEX},@{FIELD_DESC})";

        private static readonly String REQ_DELETE = $"delete from {TABLE_NAME} where {FIELD_ID} = @{FIELD_ID}";

        private static readonly string REQ_GET_BY_CONTINENT = REQ_QUERY + $" WHERE {FIELD_IDEX} = @{FIELD_IDEX}";
        
        public static IEnumerable<paysDTO> QueryPays()
        {
            List<paysDTO> pays = new List<paysDTO>();

            using (SqlConnection connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_QUERY;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    pays.Add(new paysDTO(reader));   
                }
            }
            
            return pays;
        }
        
        public static paysDTO GetPays(int id)
        {
            using (var connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
    
                command.CommandText = REQ_GET;
    
                command.Parameters.AddWithValue($"@{FIELD_ID}", id);
    
                SqlDataReader reader = command.ExecuteReader();
                return reader.Read() ? new paysDTO(reader) : null;
            }
        }
        
        public static paysDTO PostPays(paysDTO pays) 
        {
            using (SqlConnection connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_POST;

                command.Parameters.AddWithValue($@"{FIELD_NOM}", pays.Nom_pays);
                command.Parameters.AddWithValue($@"{FIELD_IDEX}", pays.Id_Continent_Pays);
                command.Parameters.AddWithValue($@"{FIELD_DESC}", pays.Desc_pays);
               
                
                pays.Id_Pays = (int) command.ExecuteScalar();
                
            }    
            return pays;
        }
        
        public static bool DeletePays(int id_pays)
        {
            bool hasBeenDeleted = false;

            using (SqlConnection connection= DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_DELETE;

                command.Parameters.AddWithValue($"@{FIELD_ID}", id_pays);
                hasBeenDeleted = command.ExecuteNonQuery() == 1; 
            }

            return hasBeenDeleted;
        }

        public static IEnumerable<paysDTO> GetPaysByContinent(int idContinent)
        {
            List<paysDTO> pays2 = new List<paysDTO>();
            
            using (SqlConnection connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
    
                command.CommandText = REQ_GET_BY_CONTINENT;
                command.Parameters.AddWithValue($"@{FIELD_IDEX}", idContinent);
    
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    pays2.Add(new paysDTO(reader));
                }

                return pays2;
            }
        }
    }
}