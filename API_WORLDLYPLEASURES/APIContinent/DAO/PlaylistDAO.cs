﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using APIContinent.DTO;
using APIContinent.Model;

namespace APIContinent.DAO
{
    public class playlistDAO
    {
        private static readonly string TABLE_NAME = "Playlist";

        public static string FIELD_ID = "Id_Playlist";
        public static string FIELD_NOM = "Nom_Playlist";
        public static string FIELD_IDUTIL = "Id_Membre";
        public static string FIELD_PLAT = "Id_Plat";
        
        private static readonly String REQ_QUERY = $"select * from {TABLE_NAME}";

        
        private static readonly String REQ_DELETE = $"delete from {TABLE_NAME} where {FIELD_ID} = @{FIELD_ID}";
        
        private static readonly string REQ_POST = $"INSERT INTO {TABLE_NAME} ({FIELD_NOM} ,{FIELD_IDUTIL}, {FIELD_PLAT}) " +
                                                  $"OUTPUT INSERTED.{FIELD_ID} " +
                                                  $"VALUES (@{FIELD_NOM}, @{FIELD_IDUTIL}, @{FIELD_PLAT})";
        
        
        
        public static IEnumerable<playlistDTO> QueryPlaylist()
        {
            List<playlistDTO> playlist = new List<playlistDTO>();

            using (SqlConnection connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_QUERY;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    playlist.Add(new playlistDTO(reader));   
                }
            }
            
            return playlist;
        }

        public static playlistDTO PostPlaylist(playlistDTO playlist)
        {
            using (SqlConnection connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_POST;

                command.Parameters.AddWithValue($@"{FIELD_NOM}", playlist.Nom_playlist);
                command.Parameters.AddWithValue($@"{FIELD_IDUTIL}", playlist.Idex1);
                command.Parameters.AddWithValue($@"{FIELD_PLAT}", playlist.Idex2);
               
                
                playlist.Id = (int) command.ExecuteScalar();
                
            }    
            return playlist;
        }


        public static bool DeletePlaylist(int id_playlist)
        {
            bool hasBeenDeleted = false;

            using (SqlConnection connection= DataBase.GetConnection() )
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_DELETE;
                command.Parameters.AddWithValue($@"{FIELD_ID}", id_playlist);
                hasBeenDeleted = command.ExecuteNonQuery() == 1;
                
            }

            return hasBeenDeleted;
        }
    }
}
