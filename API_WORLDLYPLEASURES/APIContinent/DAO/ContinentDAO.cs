﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using APIContinent.DTO;
using APIContinent.Model;

namespace APIContinent.DAO
{
    public class ContinentDAO
    {
        private static readonly string TABLE_NAME = "Continent";

        public static string FIELD_ID = "Id_Continent";
        public static string FIELD_NOM = "Nom_Continent";
        public static string FIELD_DESC = "Description_Continent";

        private static readonly string REQ_QUERY = $"select * from {TABLE_NAME}";

        private static readonly string REQ_POST = $"INSERT INTO {TABLE_NAME} ({FIELD_NOM},{FIELD_DESC}) " +
                                                  $"OUTPUT INSERTED.{FIELD_ID} " +
                                                  $"VALUES (@{FIELD_NOM},@{FIELD_DESC})";

        private static readonly string REQ_DELETE = $"delete from {TABLE_NAME} where {FIELD_ID} = @{FIELD_ID}";
        private static readonly string REQ_GET = REQ_QUERY + $" WHERE {FIELD_ID} = @{FIELD_ID}";
        public static continentDTO POSTContinent(continentDTO continent) 
        {
            using (SqlConnection connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_POST;

                command.Parameters.AddWithValue($@"{FIELD_NOM}", continent.Nom_continent);
                command.Parameters.AddWithValue($@"{FIELD_DESC}", continent.Description_continent);
               
                
                continent.Id_continent = (int) command.ExecuteScalar();
                
            }    
            return continent;
        }
        public static continentDTO get(int id)
        {
            using (var connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
    
                command.CommandText = REQ_GET;
    
                command.Parameters.AddWithValue($"@{FIELD_ID}", id);
    
                SqlDataReader reader = command.ExecuteReader();
                return reader.Read() ? new continentDTO(reader) : null;
            }
        }
        
        public static IEnumerable<continentDTO> QueryContinent()
        {
            List<continentDTO> continent = new List<continentDTO>();

            using (SqlConnection connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_QUERY;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    continent.Add(new continentDTO(reader));   
                }
            }
            
            return continent;
        }
        
        public static bool DeleteContinent(int id_continent)
        {
            bool hasBeenDeleted = false;

            using (SqlConnection connection= DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_DELETE;

                command.Parameters.AddWithValue($"@{FIELD_ID}", id_continent);
                hasBeenDeleted = command.ExecuteNonQuery() == 1; 
            }

            return hasBeenDeleted;
        }
        
    }
}