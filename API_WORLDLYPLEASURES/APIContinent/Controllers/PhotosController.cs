﻿using System.Collections.Generic;
using APIContinent.DAO;
using APIContinent.DTO;
using Microsoft.AspNetCore.Mvc;

namespace APIContinent.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PhotosController : ControllerBase
    {
        [HttpGet]
        public IEnumerable<photoDTO> Get()
        {
            return photoDAO.QueryPhotos();
        }
        
        [HttpPost]
        public photoDTO Post([FromBody] photoDTO photo)
        {
            return photoDAO.PostPhotos(photo);
        }
        
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            if (photoDAO.DeletePhotos(id))
            {
                return Ok();
            }
            return BadRequest();
        }
    }
}