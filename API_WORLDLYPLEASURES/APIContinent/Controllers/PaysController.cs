﻿using System.Collections.Generic;
using APIContinent.DAO;
using APIContinent.DTO;
using Microsoft.AspNetCore.Mvc;

namespace APIContinent.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PaysController : ControllerBase
    {
        [HttpGet]
        public IEnumerable<paysDTO> Get()
        {
            return paysDAO.QueryPays();
        }
        
  /*      [HttpGet("{id}")]
        public ActionResult<paysDTO> GetById(int id)
        {
            paysDTO pays = paysDAO.GetPays(id);
            return pays != null ? (ActionResult<paysDTO>) Ok(pays) : NotFound("this pays does not exist");
        } */
        
        [HttpPost]
        public paysDTO Post([FromBody]paysDTO paysDto)
        {
            return paysDAO.PostPays(paysDto);
        }
        
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            if (paysDAO.DeletePays(id))
            {
                return Ok();
            }
            return BadRequest();
        }
        
       [HttpGet("{idContinent}")]
        public IEnumerable<paysDTO> GetByContinent(int idContinent)
        {
            return paysDAO.GetPaysByContinent(idContinent);
        }
    }
}