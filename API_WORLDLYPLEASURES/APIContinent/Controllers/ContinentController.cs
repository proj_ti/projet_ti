﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using APIContinent.DAO;
using APIContinent.DTO;

namespace APIContinent.Controllers
{
    [ApiController]
        [Route("api/[controller]")]
        public class  ContinentController : ControllerBase
        {
            [HttpGet]
            public IEnumerable<continentDTO> Get()
            {
                return ContinentDAO.QueryContinent();
            }
            
            [HttpPost]
            public continentDTO Post([FromBody]continentDTO continentDto)
            {
                return ContinentDAO.POSTContinent(continentDto);
            }
            //coucou
            [HttpDelete("{id}")]
            public ActionResult Delete(int id)
            {
                if (ContinentDAO.DeleteContinent(id))
                {
                    return Ok();
                }
                return BadRequest();
            }
            [HttpGet("{id}")]
            public ActionResult<continentDTO> QueryById(int id)
            {
                continentDTO continent = ContinentDAO.get(id);
                return continent != null ? (ActionResult<continentDTO>) Ok(continent) : NotFound("this article does not exist");
            }
        }
    }
