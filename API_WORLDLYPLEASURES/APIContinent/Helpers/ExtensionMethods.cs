﻿using System.Collections.Generic;
using System.Linq;
using APIContinent.DTO;
using APIContinent.Entitites;

namespace APIContinent.Helpers
{
    public static class ExtensionMethods
    {
        public static IEnumerable<MembreDTO> WithoutPasswords(this IEnumerable<MembreDTO> users) {
                return users.Select(x => x.WithoutPassword());
            }

            public static MembreDTO WithoutPassword(this MembreDTO user) {
                user.Password_membre = null;
                return user;
            }
        }
    }
    