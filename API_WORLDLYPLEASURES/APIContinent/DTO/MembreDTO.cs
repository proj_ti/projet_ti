﻿using System;
using System.Data.SqlClient;
using System.Dynamic;
using APIContinent.DAO;

namespace APIContinent.DTO
{
    public class MembreDTO
    {
        public int Id { get; set; }
        public string Nom_membre { get; set; }
        public string Prenom_membre { get; set; }
        public string Pseudo_membre { get; set; }
        public string Mail_membre { get; set; }
        public string Password_membre { get; set; }
        public string Token { get; set; }

        public MembreDTO(int id, string nomMembre, string prenomMembre, string pseudoMembre, string mailMembre, string passwordMembre)
        {
            Id = id;
            Nom_membre = nomMembre;
            Prenom_membre = prenomMembre;
            Pseudo_membre = pseudoMembre;
            Mail_membre = mailMembre;
            Password_membre = passwordMembre;
        }

        public MembreDTO()
        {
        }

        public MembreDTO(SqlDataReader reader2)
        {
            Id = Convert.ToInt32(reader2[MembreDAO.FIELD_ID].ToString());
            Nom_membre = reader2[MembreDAO.FIELD_NOM].ToString();
            Prenom_membre = reader2[MembreDAO.FIELD_PRENOM].ToString();
            Pseudo_membre = reader2[MembreDAO.FIELD_PSEUDO].ToString();
            Mail_membre = reader2[MembreDAO.FIELD_PSEUDO].ToString();
            Password_membre = reader2[MembreDAO.FIELD_PASSWORD].ToString();
        }
        
    }
}