﻿using System;
using System.Data.SqlClient;
using APIContinent.DAO;

namespace APIContinent.DTO
{
    public class platDTO
    {
        public int Id_plat { get; set; }
        public string Nom_plat { get; set; }
        public string Ingredient_plat { get; set; }
        public string Recette_plat { get; set; }
        public int Id_pays_plat { get; set; }


        public platDTO(int idPlat, string nomPlat, string ingredientPlat, string recettePlat, int idPaysPlat)
        {
            Id_plat = idPlat;
            Nom_plat = nomPlat;
            Ingredient_plat = ingredientPlat;
            Recette_plat = recettePlat;
            Id_pays_plat = idPaysPlat;
        }

        public platDTO()
        {
        }

        public platDTO(SqlDataReader reader4)
        {
            Id_plat = Convert.ToInt32(reader4[platDAO.FIELD_ID].ToString());
            Nom_plat = reader4[platDAO.FIELD_NOM].ToString();
            Ingredient_plat = reader4[platDAO.FIELD_INGREDIENT].ToString();
            Recette_plat = reader4[platDAO.FIELD_RECETTE].ToString();
            Id_pays_plat = Convert.ToInt32(reader4[platDAO.FIELD_EXT].ToString());
        }
    }
}