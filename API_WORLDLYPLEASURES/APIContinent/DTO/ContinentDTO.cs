﻿using System;
using System.Data.SqlClient;
using APIContinent.DAO;

namespace APIContinent.DTO
{
    public class continentDTO
    {
       public int Id_continent { get; set; }

       public string Nom_continent { get; set; }
       
       public string Description_continent { get; set; }

       public continentDTO(int idContinent, string nomContinent, string descriptionContinent)
       {
           Id_continent = idContinent;
           Nom_continent = nomContinent;
           Description_continent = descriptionContinent;
       }

       public continentDTO()
       {
       }

       public continentDTO(SqlDataReader reader)
       {
           Id_continent = Convert.ToInt32(reader[ContinentDAO.FIELD_ID].ToString());
           Nom_continent = reader[ContinentDAO.FIELD_NOM].ToString();
           Description_continent = reader[ContinentDAO.FIELD_DESC].ToString();

       }
    }
}