﻿using System;
using System.Data.SqlClient;
using APIContinent.DAO;

namespace APIContinent.DTO
{
    public class playlistDTO
    {
        public int Id { get; set;}
        public string Nom_playlist { get; set; }
        public int Idex1 { get; set;}
        public int Idex2 { get; set;}


        public playlistDTO(int id, string nomPlaylist, int idex1, int idex2)
        {
            Id = id;
            Nom_playlist = nomPlaylist;
            Idex1 = idex1;
            Idex2 = idex2;
        }

        public playlistDTO()
        {
        }

        public playlistDTO(SqlDataReader reader)
        {
            Id = Convert.ToInt32(reader[playlistDAO.FIELD_ID].ToString());
            Nom_playlist = reader[playlistDAO.FIELD_NOM].ToString();
            Idex1 = Convert.ToInt32(reader[playlistDAO.FIELD_IDUTIL].ToString());
            Idex2 = Convert.ToInt32(reader[playlistDAO.FIELD_PLAT].ToString());
        }
    }
}