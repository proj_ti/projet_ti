﻿using System;
using System.Data.SqlClient;
using APIContinent.DAO;

namespace APIContinent.DTO
{
    public class paysDTO
    {
        public int Id_Pays { get; set; }
        public string Nom_pays { get; set; }
        public string Desc_pays { get; set;}
        public int Id_Continent_Pays { get; set;}

        public paysDTO(int idPays, string nomPays, string descPays, int idContinentPays)
        {
            Id_Pays = idPays;
            Nom_pays = nomPays;
            Desc_pays = descPays;
            Id_Continent_Pays = idContinentPays;
        }

        public paysDTO()
        {
        }

        public paysDTO(SqlDataReader reader3)
        {
            Id_Pays = Convert.ToInt32(reader3[paysDAO.FIELD_ID].ToString());
            Nom_pays = reader3[paysDAO.FIELD_NOM].ToString();
            Id_Continent_Pays = Convert.ToInt32(reader3[paysDAO.FIELD_IDEX].ToString());
            Desc_pays = reader3[paysDAO.FIELD_DESC].ToString();
        }
    }
}