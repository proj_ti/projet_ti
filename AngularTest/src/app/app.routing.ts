import { Routes, RouterModule } from '@angular/router';

import {IndexComponent} from './Pages/Index/index.component';
import {InscriptionComponent} from './Pages/inscription/inscription.component';
import {ConnexionComponent} from './Pages/connexion/connexion.component';
import {ContinentComponent} from './Pages/continent/continent.component';
import {PaysComponent} from './Pages/pays/pays.component';
import {PlaylistComponent} from './Pages/playlist/playlist.component';

const routes: Routes = [
  { path: '', component: IndexComponent },
  { path: 'connexion', component: ConnexionComponent },
  { path: 'inscription', component: InscriptionComponent },
  { path: 'continent/:id', component: ContinentComponent },
  { path: 'pays/:id', component: PaysComponent },
  { path: 'playlist', component: PlaylistComponent },

  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

export const appRoutingModule = RouterModule.forRoot(routes);
