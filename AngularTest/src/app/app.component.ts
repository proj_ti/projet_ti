import { Component } from '@angular/core';
import {ContinentDTO} from './interface-worldyPleasures/continentDTO';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'worldlyPleasures';
  private continents : ContinentDTO[] = [];

  constructor() {}
  nfOnInit() {
  }

}
