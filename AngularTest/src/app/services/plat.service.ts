import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {PaysList} from '../interface-worldyPleasures/pays';
import {PaysDTO} from '../interface-worldyPleasures/paysDTO';
import {PlatList} from '../interface-worldyPleasures/plat';
import {PlatDTO} from '../interface-worldyPleasures/platDTO';

const URL_API: string = './api/plat';

@Injectable({
  providedIn: 'root'
})
export class PlatService {

  constructor(public http : HttpClient) { }

  query(): Observable<PlatList> {
    return this.http.get<PlatList>(URL_API);
  }

  get(idPays : number): Observable<PlatDTO> {
    return this.http.get<PlatDTO>(URL_API + '/' + idPays);
  }

  post(plat: PlatDTO): Observable<PlatDTO> {
    return this.http.post<PlatDTO>(URL_API, plat);
  }

  delete(id: number): Observable<any> {
    return this.http.delete(URL_API + '/' + id);
  }

  put(plat: PlatDTO): Observable<any> {
    return this.http.put(URL_API, plat);
  }

}

