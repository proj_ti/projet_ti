import { Injectable } from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {Membre} from '../interface-worldyPleasures/membre';
import {membreDTO} from '../interface-worldyPleasures/membreDTO';

@Injectable({
  providedIn: 'root'
})
export class MembrecreerService {
  private subject: Subject<membreDTO> = new Subject<membreDTO>();
  public $membreCreer: Observable<membreDTO> = this.subject.asObservable();


  constructor() { }

  notify(membre: membreDTO): void{
    this.subject.next(membre);
  }
}
