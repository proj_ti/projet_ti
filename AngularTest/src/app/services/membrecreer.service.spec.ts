import { TestBed } from '@angular/core/testing';

import { MembrecreerService } from './membrecreer.service';

describe('MembrecreerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MembrecreerService = TestBed.get(MembrecreerService);
    expect(service).toBeTruthy();
  });
});
