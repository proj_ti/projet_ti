import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionTitrePaysCardComponent } from './section-titre-pays-card.component';

describe('SectionTitrePaysCardComponent', () => {
  let component: SectionTitrePaysCardComponent;
  let fixture: ComponentFixture<SectionTitrePaysCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SectionTitrePaysCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionTitrePaysCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
