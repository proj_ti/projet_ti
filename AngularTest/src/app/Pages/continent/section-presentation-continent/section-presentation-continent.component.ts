import {Component, Input, OnInit} from '@angular/core';
import {ContinentDTO} from '../../../interface-worldyPleasures/continentDTO';

@Component({
  selector: 'app-section-presentation-continent',
  templateUrl: './section-presentation-continent.component.html',
  styleUrls: ['./section-presentation-continent.component.css']
})
export class SectionPresentationContinentComponent implements OnInit {

  private _continent : ContinentDTO;

  constructor() { }

  ngOnInit() {
  }

  get continent(): ContinentDTO {
    return this._continent;
  }

  @Input()
  set continent(value: ContinentDTO) {
    this._continent = value;
  }
}
