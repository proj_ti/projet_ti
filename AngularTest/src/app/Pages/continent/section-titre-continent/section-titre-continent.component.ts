import {Component, Input, OnInit} from '@angular/core';
import {ContinentDTO} from '../../../interface-worldyPleasures/continentDTO';

@Component({
  selector: 'app-section-titre-continent',
  templateUrl: './section-titre-continent.component.html',
  styleUrls: ['./section-titre-continent.component.css']
})
export class SectionTitreContinentComponent implements OnInit {

  private _continent : ContinentDTO;

  constructor() { }

  ngOnInit() {
  }

  get continent(): ContinentDTO {
    return this._continent;
  }

  @Input()
  set continent(value: ContinentDTO) {
    this._continent = value;
  }
}
