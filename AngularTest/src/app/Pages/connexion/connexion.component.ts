import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
// A supprimer Validators.email & minLength, c'est lors de l'inscription seulement
export class ConnexionComponent implements OnInit {
  form: FormGroup = this.fb.group({
    mailConnexion: this.fb.control('', [Validators.required, Validators.email]),
    motDePasseConnexion: this.fb.control('', [Validators.required, Validators.minLength(8)])
  });
  submitted = false;

  constructor(public fb : FormBuilder) { }

  ngOnInit() {
  }

  // getter qui reprend ce que contient le form
  get f() { return this.form.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.form.invalid) {
      return;
    }
  }

}
