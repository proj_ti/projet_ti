import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionTitreMenuComponent } from './section-titre-menu.component';

describe('SectionTitreMenuComponent', () => {
  let component: SectionTitreMenuComponent;
  let fixture: ComponentFixture<SectionTitreMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SectionTitreMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionTitreMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
