import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionPresentationSiteComponent } from './section-presentation-site.component';

describe('SectionPresentationSiteComponent', () => {
  let component: SectionPresentationSiteComponent;
  let fixture: ComponentFixture<SectionPresentationSiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SectionPresentationSiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionPresentationSiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
