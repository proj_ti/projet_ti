import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionPresentationContinentCardComponent } from './section-presentation-continent-card.component';

describe('SectionPresentationContinentCardComponent', () => {
  let component: SectionPresentationContinentCardComponent;
  let fixture: ComponentFixture<SectionPresentationContinentCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SectionPresentationContinentCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionPresentationContinentCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
