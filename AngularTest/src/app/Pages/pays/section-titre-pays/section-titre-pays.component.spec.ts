import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionTitrePaysComponent } from './section-titre-pays.component';

describe('SectionTitrePaysComponent', () => {
  let component: SectionTitrePaysComponent;
  let fixture: ComponentFixture<SectionTitrePaysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SectionTitrePaysComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionTitrePaysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
