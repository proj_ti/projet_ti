import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionPresentationPaysComponent } from './section-presentation-pays.component';

describe('SectionPresentationPaysComponent', () => {
  let component: SectionPresentationPaysComponent;
  let fixture: ComponentFixture<SectionPresentationPaysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SectionPresentationPaysComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionPresentationPaysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
