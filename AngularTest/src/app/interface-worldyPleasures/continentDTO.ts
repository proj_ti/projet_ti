export interface ContinentDTO {
  id_continent: number;
  nom_continent: string;
  description_continent: string;
}
