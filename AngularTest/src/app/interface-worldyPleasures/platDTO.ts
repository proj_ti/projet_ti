export interface PlatDTO {
  id_plat : number;
  nom_plat : string;
  ingredient_plat : string;
  recette_plat : string;
  id_pays_plat : number;
}
