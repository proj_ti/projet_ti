import {membreDTO} from './membreDTO';

export  declare type membreList = Membre[];

export class Membre {

  private _id_membre:number;
  private _nom_membre:string;
  private _prenom_membre:string;
  private _pseudo_membre:string;
  private _mail_membre:string;
  private _password_membre:string;


  constructor(id_membre: number = -1, nom_membre: string ='', prenom_membre: string ='', pseudo_membre: string ='', mail_membre: string ='', password_membre: string ='') {
    this._id_membre = id_membre;
    this._nom_membre = nom_membre;
    this._prenom_membre = prenom_membre;
    this._pseudo_membre = pseudo_membre;
    this._mail_membre = mail_membre;
    this._password_membre = password_membre;
  }

  toMembreDTO(): membreDTO {
    return {
      id: this._id_membre,
      nom_membre:this._nom_membre,
      prenom_membre:this._prenom_membre,
      pseudo_membre:this._pseudo_membre,
      mail_membre:this._mail_membre,
      password_membre:this._password_membre
    }
  }
  get id_membre(): number {
    return this._id_membre;
  }

  get nom_membre(): string {
    return this._nom_membre;
  }

  get prenom_membre(): string {
    return this._prenom_membre;
  }

  get pseudo_membre(): string {
    return this._pseudo_membre;
  }

  get mail_membre(): string {
    return this._mail_membre;
  }

  get password_membre(): string {
    return this._password_membre;
  }


  set id_membre(value: number) {
    this._id_membre = value;
  }

  set nom_membre(value: string) {
    this._nom_membre = value;
  }

  set prenom_membre(value: string) {
    this._prenom_membre = value;
  }

  set pseudo_membre(value: string) {
    this._pseudo_membre = value;
  }

  set mail_membre(value: string) {
    this._mail_membre = value;
  }

  set password_membre(value: string) {
    this._password_membre = value;
  }

  equals(obj):boolean{
    if (obj instanceof Membre){
      return this._id_membre === (<Membre>obj)._id_membre;
    }
    return false;
  }
}
