import {ContinentDTO} from './continentDTO';
import {Observable} from 'rxjs';

export declare type  ContinentList = Continent[];

export class Continent{
  private _id_continent:number;
  private _nom_continent:string;
  private _description_continent:string;
  constructor(id: number = -1, nom: string = '',desc:string=''){
    this._id_continent = id;
    this._nom_continent = nom;
    this._description_continent = desc;
  }

  toContinentDTO(): ContinentDTO{
    return {
      id_continent: this._id_continent,
      nom_continent: this._nom_continent,
      description_continent:this._description_continent
    };
  }

  fromContinentDTO(dto: Continent): Continent {
    Object.assign(this, dto);
    return this;
  }

  set description_continent(value: string) {
    this._description_continent = value;
  }

  get description_continent(): string {
    return this._description_continent;
  }

  get id_continent(): number{
    return this._id_continent;
  }

  get nom_continent(): string{
    return this._nom_continent;
  }

  set id_continent(value: number){
    this._id_continent = value;
  }

  set nom_continent(value: string){
    this._nom_continent = value;
  }

  equals(obj): boolean {
    if (obj instanceof Continent) {
      return this._id_continent === (<Continent>obj)._id_continent;
    }
    return false;
  }

}

