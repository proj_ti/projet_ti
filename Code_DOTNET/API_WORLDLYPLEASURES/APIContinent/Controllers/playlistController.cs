﻿using System.Collections.Generic;
using APIContinent.DAO;
using APIContinent.DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;

namespace APIContinent.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class playlistController : ControllerBase
    {
        [HttpGet]
        public IEnumerable<playlistDTO> Get()
        {
            return playlistDAO.QueryPlaylist();
        }
        
        [HttpPost]
        public playlistDTO Post([FromBody]playlistDTO playlist)
        {
            return playlistDAO.PostPlaylist(playlist);
        }
        
        
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
           if (playlistDAO.DeletePlaylist(id))
           {
               return Ok();
           }
           return BadRequest();
        }
    }
}