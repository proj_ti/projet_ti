﻿using System.Collections.Generic;
using APIContinent.DAO;
using APIContinent.DTO;
using Microsoft.AspNetCore.Mvc;

namespace APIContinent.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PaysController : ControllerBase
    {
        [HttpGet]
        public IEnumerable<paysDTO> Get()
        {
            return paysDAO.QueryPays();
        }
        
        [HttpPost]
        public paysDTO Post([FromBody]paysDTO paysDto)
        {
            return paysDAO.PostPays(paysDto);
        }
        
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            if (paysDAO.DeletePays(id))
            {
                return Ok();
            }
            return BadRequest();
        }
    }
}