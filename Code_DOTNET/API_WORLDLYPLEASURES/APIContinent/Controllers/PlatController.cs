﻿using System.Collections.Generic;
using APIContinent.DAO;
using APIContinent.DTO;
using Microsoft.AspNetCore.Mvc;

namespace APIContinent.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PlatController : ControllerBase
    {
        [HttpGet]
        public IEnumerable<platDTO> Get()
        {
            return platDAO.QueryPlat();
        }
        
        [HttpPost]
        public platDTO Post([FromBody] platDTO plat)
        {
            return platDAO.PostPlat(plat);
        }
        
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            if (platDAO.DeletePlat(id))
            {
                return Ok();
            }
            return BadRequest();
        }
    }
}