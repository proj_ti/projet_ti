﻿using System.Collections.Generic;
using APIContinent.DAO;
using APIContinent.DTO;
using Microsoft.AspNetCore.Mvc;

namespace APIContinent.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MembreController : ControllerBase
    {
        [HttpGet]
        public IEnumerable<MembreDTO> Get()
        {
            return MembreDAO.QueryMembre();
        }
        [HttpPost]
        public MembreDTO Post([FromBody]MembreDTO membre)
        {
            return MembreDAO.PostMembre(membre);
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            if (MembreDAO.DeleteMembre(id))
            {
                return Ok();
            }
            return BadRequest();
        }
    }
}