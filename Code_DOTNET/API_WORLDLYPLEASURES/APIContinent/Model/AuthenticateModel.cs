﻿using System.ComponentModel.DataAnnotations;

namespace APIContinent.Model
{
    public class AuthenticateModel
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }
}