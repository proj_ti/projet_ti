﻿using System;
using System.Data.SqlClient;
using APIContinent.DAO;

namespace APIContinent.DTO
{
    public class platDTO
    {
        public int Id { get; set; }
        public string Nom_plat { get; set; }
        public string Ingredient_plat { get; set; }
        public string Recette_plat { get; set; }
        public int Id_pays { get; set; }


        public platDTO(int id, string nomPlat, string ingredientPlat, string recettePlat, int idPays)
        {
            Id = id;
            Nom_plat = nomPlat;
            Ingredient_plat = ingredientPlat;
            Recette_plat = recettePlat;
            Id_pays = idPays;
        }

        public platDTO()
        {
        }

        public platDTO(SqlDataReader reader4)
        {
            Id = Convert.ToInt32(reader4[platDAO.FIELD_ID].ToString());
            Nom_plat = reader4[platDAO.FIELD_NOM].ToString();
            Ingredient_plat = reader4[platDAO.FIELD_INGREDIENT].ToString();
            Recette_plat = reader4[platDAO.FIELD_RECETTE].ToString();
            Id_pays = Convert.ToInt32(reader4[platDAO.FIELD_EXT].ToString());
        }
    }
}