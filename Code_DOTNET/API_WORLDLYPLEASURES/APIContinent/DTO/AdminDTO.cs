﻿using System;
using System.Data.SqlClient;
using APIContinent.DAO;

namespace APIContinent.DTO
{
    public class AdminDTO
    {
        public int Id { get; set; }
        public int Idex { get; set; }

        public AdminDTO()
        {
        }

        public AdminDTO(int id, int idex)
        {
            Id = id;
            Idex = idex;
        }

        public AdminDTO(SqlDataReader reader)
        {
            Id = Convert.ToInt32(reader[AdminDAO.FIELD_ID].ToString());
            Idex = Convert.ToInt32(reader[AdminDAO.FIELD_EXE].ToString());
        }
    }
}