﻿using System;
using System.Data.SqlClient;
using APIContinent.DAO;

namespace APIContinent.DTO
{
    public class paysDTO
    {
        public int Id { get; set; }
        public string Nom_pays { get; set; }
        public int Idex { get; set;}

        public paysDTO(int id, string nomPays, int idex)
        {
            Id = id;
            Nom_pays = nomPays;
            Idex = idex;
        }

        public paysDTO()
        {
        }

        public paysDTO(SqlDataReader reader3)
        {
            Id = Convert.ToInt32(reader3[paysDAO.FIELD_ID].ToString());
            Nom_pays = reader3[paysDAO.FIELD_NOM].ToString();
            Idex = Convert.ToInt32(reader3[paysDAO.FIELD_IDEX].ToString());
        }
    }
}