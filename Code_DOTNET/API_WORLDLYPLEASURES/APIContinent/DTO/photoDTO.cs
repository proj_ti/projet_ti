﻿using System;
using System.Data.SqlClient;
using APIContinent.DAO;

namespace APIContinent.DTO
{
    public class photoDTO
    {
        public int Id { get; set; }
        public int Idex { get; set; }
        public string Nom_photo { get; set; }
        public string Chemin_photo { get; set; }

        public photoDTO(int id, int idex, string nomphoto, string cheminphoto)
        {
            Id = id;
            Idex = idex;
            Nom_photo = nomphoto;
            Chemin_photo = cheminphoto;
        }

        public photoDTO()
        {
        }

        public photoDTO(SqlDataReader reader)
        {
            Id = Convert.ToInt32(reader[photoDAO.FIELD_ID].ToString());
            Idex = Convert.ToInt32(reader[photoDAO.FIELD_EXE].ToString());
            Nom_photo = reader[photoDAO.FIELD_NOM].ToString();
            Chemin_photo = reader[photoDAO.FIELD_CHEMIN].ToString();
        }
    }
}