﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using APIContinent.DTO;
using APIContinent.Model;

namespace APIContinent.DAO
{
    public class platDAO
    {
        private static readonly string TABLE_NAME = "Recette";

        public static string FIELD_ID = "Id_Plat";
        public static string FIELD_NOM = "Nom_Plat";
        public static string FIELD_INGREDIENT = "Ingredient_Plat";
        public static string FIELD_RECETTE = "Recette_Plat";
        public static string FIELD_EXT = "Id_Pays"; 
        
        
        private static readonly String REQ_QUERY = $"select * from {TABLE_NAME}";

        private static readonly String REQ_POST =
            $"INSERT INTO {TABLE_NAME} ({FIELD_NOM},{FIELD_INGREDIENT},{FIELD_RECETTE},{FIELD_EXT}) " +
            $" OUTPUT INSERTED.{FIELD_ID}" +
            $" VALUES (@{FIELD_NOM},@{FIELD_INGREDIENT},@{FIELD_RECETTE},@{FIELD_EXT})";

        private static readonly String REQ_DELETE = $"delete from {TABLE_NAME} where {FIELD_ID} = @{FIELD_ID}";
        public static List<platDTO> QueryPlat()
        {
            List<platDTO> plat = new List<platDTO>();

            using (SqlConnection connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_QUERY;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    plat.Add(new platDTO(reader));   
                }
            }
            
            return plat;
        }
        
        public static platDTO PostPlat(platDTO plat) 
        {
            using (SqlConnection connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_POST;

                command.Parameters.AddWithValue($@"{FIELD_NOM}", plat.Nom_plat);
                command.Parameters.AddWithValue($@"{FIELD_INGREDIENT}", plat.Ingredient_plat);
                command.Parameters.AddWithValue($@"{FIELD_RECETTE}", plat.Recette_plat);
                command.Parameters.AddWithValue($@"{FIELD_EXT}", plat.Id_pays);
              
                plat.Id = (int) command.ExecuteScalar();
                
            }    
            return plat;
        }
        public static bool DeletePlat(int id_plat)
        {
            bool hasBeenDeleted = false;

            using (SqlConnection connection= DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_DELETE;

                command.Parameters.AddWithValue($"@{FIELD_ID}", id_plat);
                hasBeenDeleted = command.ExecuteNonQuery() == 1; 
            }

            return hasBeenDeleted;
        }
        
        
    }
}