﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using APIContinent.DTO;
using APIContinent.Model;

namespace APIContinent.DAO
{
    public class AdminDAO
    {
        private static readonly string TABLE_NAME = "Admin";
        
        public static string FIELD_ID = "Id_Admin";
        public static string FIELD_EXE = "Id_Membre";
        
        private static readonly String REQ_QUERY = $"select * from {TABLE_NAME}";
        private static readonly String REQ_DELETE = $"delete from {TABLE_NAME} where {FIELD_ID} = @{FIELD_ID}";

        
        private static readonly String REQ_POST =
            $"INSERT INTO {TABLE_NAME} ({FIELD_EXE}) " +
            $" OUTPUT INSERTED.{FIELD_ID}" +
            $" VALUES (@{FIELD_EXE})";

        public static List<AdminDTO> QueryAdmin()
        {
            List<AdminDTO> admin = new List<AdminDTO>();

            using (SqlConnection connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_QUERY;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    admin.Add(new AdminDTO(reader));   
                }
            }
            
            return admin;
        }

        
        public static AdminDTO PostAdmin(AdminDTO admin) 
        {
            using (SqlConnection connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_POST;

                command.Parameters.AddWithValue($@"{FIELD_EXE}", admin.Idex);
               
              
                admin.Id = (int) command.ExecuteScalar();
                
            }    
            return admin;
        }
        
    }
}