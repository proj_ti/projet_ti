import {Component, Input, OnInit} from '@angular/core';
import {ListContinentComponent} from '../../../continent/list-continent/list-continent.component';
import {MOCK_CONTINENT} from '../../../mock-worldlyPleasures/mock-continent';
import {ContinentDTO} from '../../../interface-worldyPleasures/continentDTO';
import {Continent, ContinentList} from '../../../interface-worldyPleasures/continent';

@Component({
  selector: 'app-section-presentation-continent-card',
  templateUrl: './section-presentation-continent-card.component.html',
  styleUrls: ['./section-presentation-continent-card.component.css']
})
export class SectionPresentationContinentCardComponent implements OnInit {

  private _continents : ContinentList = [];

  constructor() { }

  ngOnInit() {
  }

  get continents(): Continent[] {
    return this._continents;
  }

  @Input()
  set continents(value: Continent[]) {
    this._continents = value;
  }

}
