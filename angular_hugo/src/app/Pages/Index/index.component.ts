import { Component, OnInit } from '@angular/core';
import {Continent, ContinentList} from '../../interface-worldyPleasures/continent';
import {ContinentService} from '../../services/continent.service';
import {Observable, Subscription} from 'rxjs';
import {ContinentDTO} from '../../interface-worldyPleasures/continentDTO';


@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  private _continents: ContinentList = [];
  private subscriptions: Subscription[] = [];

  // @ts-ignore

  constructor(public continentService : ContinentService) { }

  ngOnInit() {
    this.loadContinent();
  }

  private loadContinent() {
    const sub = this.continentService
      .query()
      //Possible que ça fonctionne pas
      .subscribe(continents => {
        this._continents = continents.map(continent => new Continent().fromContinentDTO(continent));
        console.log(this._continents);
      });

    this.subscriptions.push(sub);
  }

  get continents(): Continent[] {
    return this._continents;
  }

  set continents(value: Continent[]) {
    this._continents = value;
  }
}
