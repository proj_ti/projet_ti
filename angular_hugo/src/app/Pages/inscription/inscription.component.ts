import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {membreDTO} from '../../interface-worldyPleasures/membreDTO';
import {MembrecreerService} from '../../services/membrecreer.service';
import {Subscription} from 'rxjs';
import {MembreService} from '../../services/membre.service';
import {Membre} from '../../interface-worldyPleasures/membre';
@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css']
})
export class InscriptionComponent implements OnInit {
  private subscriptions: Subscription[] = [];
private membres: Membre[] = [];
  form : FormGroup = this.fb.group( {
    nomInscription : this.fb.control('', Validators.required),
    prenomInscription : this.fb.control('', Validators.required),
    pseudoInscription : this.fb.control('', Validators.required),
    mailInscription : this.fb.control('', [Validators.required, Validators.email]),
    motDePasseInscription : this.fb.control('', [Validators.required, Validators.minLength(8.)])
  });
  submitted = false;
  constructor(public fb : FormBuilder, public membreService: MembreService,public membrecreer: MembrecreerService) { }

  ngOnInit() {
    this.listenToNewMembersCreated();
  }


  notifyNewTodo() {

    this.membrecreer.notify(this.buildMembre());
    console.log(this.buildMembre());
    this.form.reset();
  }

  buildMembre(log: void): membreDTO{
    return {
      nom_membre: this.form.get('nomInscription').value,
      prenom_membre: this.form.get('prenomInscription').value,
      pseudo_membre:this.form.get('pseudoInscription').value,
      mail_membre:this.form.get('mailInscription').value,
      password_membre:this.form.get('motDePasseInscription').value
    };
  }

  private listenToNewMembersCreated() {
    const sub: Subscription = this.membrecreer
      .$membreCreer
      .subscribe(membreCreer => this.createMembre(membreCreer));
    this.subscriptions.push(sub);
  }

  private createMembre(membre) {
      const sub = this.membreService.post(membre).subscribe(membreFromDb => this.membres.push(<Membre> <membreDTO> membreFromDb));
      this.subscriptions.push(sub);
  }
}
