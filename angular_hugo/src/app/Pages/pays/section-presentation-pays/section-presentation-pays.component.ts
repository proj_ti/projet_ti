import {Component, Input, OnInit} from '@angular/core';
import {PaysDTO} from '../../../interface-worldyPleasures/paysDTO';

@Component({
  selector: 'app-section-presentation-pays',
  templateUrl: './section-presentation-pays.component.html',
  styleUrls: ['./section-presentation-pays.component.css']
})
export class SectionPresentationPaysComponent implements OnInit {
  private _pays : PaysDTO;

  constructor() { }

  ngOnInit() {
  }

  get pays(): PaysDTO{
    return this._pays;
  }

  @Input()
  set pays(value: PaysDTO){
    this._pays = value;
  }

}
