import {Component, Input, OnInit} from '@angular/core';
import {PaysDTO} from '../../../interface-worldyPleasures/paysDTO';

@Component({
  selector: 'app-section-titre-pays',
  templateUrl: './section-titre-pays.component.html',
  styleUrls: ['./section-titre-pays.component.css']
})
export class SectionTitrePaysComponent implements OnInit {
  private _pays : PaysDTO;

  constructor() { }

  ngOnInit() {
  }

  get pays(): PaysDTO{
    return this._pays;
  }

  @Input()
  set pays(value: PaysDTO){
    this._pays = value;
  }
}
