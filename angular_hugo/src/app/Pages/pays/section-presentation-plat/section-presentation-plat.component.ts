import {Component, Input, OnInit} from '@angular/core';
import {ContinentDTO} from '../../../interface-worldyPleasures/continentDTO';
import {PlatDTO} from '../../../interface-worldyPleasures/platDTO';

@Component({
  selector: 'app-section-presentation-plat',
  templateUrl: './section-presentation-plat.component.html',
  styleUrls: ['./section-presentation-plat.component.css']
})
export class SectionPresentationPlatComponent implements OnInit {

  private _plat : PlatDTO;

  constructor() { }

  ngOnInit() {
  }

  get plat(): PlatDTO {
    return this._plat;
  }

  @Input()
  set plat(value: PlatDTO) {
    this._plat = value;
  }
}
