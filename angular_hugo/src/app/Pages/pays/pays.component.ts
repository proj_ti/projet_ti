import { Component, OnInit } from '@angular/core';
import {Pays} from '../../interface-worldyPleasures/pays';
import {Pays2Service} from '../../services/pays2.service';
import {PlatService} from '../../services/plat.service';
import {ActivatedRoute} from '@angular/router';
import {PaysDTO} from '../../interface-worldyPleasures/paysDTO';
import {PlatDTO} from '../../interface-worldyPleasures/platDTO';

@Component({
  selector: 'app-pays',
  templateUrl: './pays.component.html',
  styleUrls: ['./pays.component.css']
})
export class PaysComponent implements OnInit {

  private _pays : PaysDTO;
  private _plat : PlatDTO;
  private _idPays: number;

  constructor(public paysService : Pays2Service,
              public platService : PlatService,
              public activatedR : ActivatedRoute,) { }

  ngOnInit() {
    let that = this;
    this._idPays = this.activatedR.snapshot.params.id;
    console.log(this._idPays);
    this.paysService
      .getUnique(this._idPays)
      .subscribe(function whenPaysFound(pays) {
        console.log(pays);
        return that._pays = pays;
      });
    this.platService
      .get(this._idPays)
      .subscribe(function whenPlatFound(plat) {
        console.log(plat);
        return that._plat = plat;
      });
  }

  get pays(): PaysDTO {
    return this._pays;
  }

  set pays(value: PaysDTO) {
    this._pays = value;
  }

  get idPays(): number {
    return this._idPays;
  }

  set idPays(value: number) {
    this._idPays = value;
  }

  get plat(): PlatDTO {
    return this._plat;
  }

  set plat(value: PlatDTO) {
    this._plat = value;
  }

}
