import {Component, Input, OnInit} from '@angular/core';
import {Pays} from '../../../interface-worldyPleasures/pays';

@Component({
  selector: 'app-section-titre-pays-card',
  templateUrl: './section-titre-pays-card.component.html',
  styleUrls: ['./section-titre-pays-card.component.css']
})
export class SectionTitrePaysCardComponent implements OnInit {

  private _listPays : Pays[];

  constructor() { }

  ngOnInit() {
  }

  get listPays(): Pays[] {
    return this._listPays;
  }
  @Input()
  set listPays(value: Pays[]) {
    this._listPays = value;
  }
}
