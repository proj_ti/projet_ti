import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionTitreContinentComponent } from './section-titre-continent.component';

describe('SectionTitreContinentComponent', () => {
  let component: SectionTitreContinentComponent;
  let fixture: ComponentFixture<SectionTitreContinentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SectionTitreContinentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionTitreContinentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
