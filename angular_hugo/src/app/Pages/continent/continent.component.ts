import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {Continent} from '../../interface-worldyPleasures/continent';
import {ContinentService} from '../../services/continent.service';
import {Observable} from 'rxjs';
import {switchMap} from 'rxjs/operators';
import {ContinentDTO} from '../../interface-worldyPleasures/continentDTO';
import {PaysService} from '../../services/pays.service';
import {Pays} from '../../interface-worldyPleasures/pays';
import {PaysDTO} from '../../interface-worldyPleasures/paysDTO';

@Component({
  selector: 'app-continent',
  templateUrl: './continent.component.html',
  styleUrls: ['./continent.component.css']
})
export class ContinentComponent implements OnInit {

  sub;
  private _observableContinents: Observable<ContinentDTO>;
  private _continent : ContinentDTO;
  private _idContinent: any;
  private _listPays : Pays[];

  continents$: Observable<Continent>;
  selectedId: number;

  constructor(public activatedR : ActivatedRoute,
              public continentService : ContinentService,
              public paysService : PaysService) { }

  ngOnInit() {
    let that = this;
    this._idContinent = this.activatedR.snapshot.params.id;
    console.log(this._idContinent);
    this._observableContinents = this.continentService.get(this._idContinent);
    this.continentService
      .get(this._idContinent)
      .subscribe(function whenContinentFound(continent) {
        console.log(continent);
        return that._continent = continent;

      });

    this.paysService
      .get(this._idContinent)
      .subscribe(listPays => {
        this._listPays = listPays.map(pays=> new Pays().fromPaysDTO(pays));
        console.log(this._listPays);

        return that._listPays = listPays;
      });




/*
    this.continents$ = this.activatedR.paramMap.pipe(
      switchMap(params => {
        // (+) before `params.get()` turns the string into a number
        this.selectedId = +params.get('id');
        return this.service.getHeroes();
      })
    );

     */
  }

  get continent(): ContinentDTO {
    return this._continent;
  }

  // Recup l'id et le mettre dedans
  set continent(value: ContinentDTO) {
    this._continent = value;
  }

}
