import {PlatDTO} from './platDTO';

export declare type PlatList = PlatDTO[];

export class Plat{

  private _idPlat : number;
  private _nomPlat : string;
  private _ingredientPlat : string;
  private _recettePlat : string;
  private _idPays : number;

  constructor(idPlat : number, nomPlat : string, ingredientPlat : string, recettePlat : string, idPays : number){
    this._idPlat = idPlat;
    this._nomPlat = nomPlat;
    this._ingredientPlat = ingredientPlat;
    this._recettePlat = recettePlat;
    this._idPays = idPays;
  }

  toPlatDTO(): PlatDTO{
    return {
      id_plat : this._idPlat,
      nom_plat : this._nomPlat,
      ingredient_plat : this._ingredientPlat,
      recette_plat : this._recettePlat,
      id_pays_plat : this._idPays
    }
  }

  fromPlatDTO(plat : PlatDTO) : Plat {
    Object.assign(this, plat);
    return this;
  }

  get idPays(): number {
    return this._idPays;
  }

  set idPays(value: number) {
    this._idPays = value;
  }
  get recettePlat(): string {
    return this._recettePlat;
  }

  set recettePlat(value: string) {
    this._recettePlat = value;
  }
  get ingredientPlat(): string {
    return this._ingredientPlat;
  }

  set ingredientPlat(value: string) {
    this._ingredientPlat = value;
  }
  get nomPlat(): string {
    return this._nomPlat;
  }

  set nomPlat(value: string) {
    this._nomPlat = value;
  }
  get idPlat(): number {
    return this._idPlat;
  }

  set idPlat(value: number) {
    this._idPlat = value;
  }

}
