export interface PaysDTO {
  id_pays: number;
  nom_pays: string;
  desc_pays: string;
  id_continent_pays: number;
}
