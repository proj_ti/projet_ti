import {PaysDTO} from './paysDTO';

export declare type PaysList = Pays[];

export class Pays {

  private _id_pays : number;
  private _nom_pays : string;
  private _desc_pays : string;
  private _id_continent_pays : number;

  constructor (idPays : number = -1, nomPays : string = '', desc_pays = '',idContinent : number = -1){
    this._id_pays = idPays;
    this._nom_pays = nomPays;
    this._desc_pays = desc_pays;
    this._id_continent_pays = idContinent;
  }

  toPaysDTO() : PaysDTO{
    return {
      id_pays : this._id_pays,
      nom_pays : this._nom_pays,
      desc_pays : this._desc_pays,
      id_continent_pays : this._id_continent_pays
    }
  }

  fromPaysDTO(pays : Pays): Pays{
    Object.assign(this, pays);
    return this;
  }

  get id_continent_pays(): number {
    return this._id_continent_pays;
  }
  set id_continent_pays(value: number) {
    this._id_continent_pays = value;
  }

  get nom_pays(): string {
    return this._nom_pays;
  }
  set nom_pays(value: string) {
    this._nom_pays = value;
  }

  get id_pays(): number {
    return this._id_pays;
  }
  set id_pays(value: number) {
    this._id_pays = value;
  }

  get desc_pays(): string {
    return this._desc_pays;
  }
  set desc_pays(value: string) {
    this._desc_pays = value;
  }
}

