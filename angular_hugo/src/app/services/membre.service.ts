import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {membreDTO} from '../interface-worldyPleasures/membreDTO';
import {Membre, membreList} from '../interface-worldyPleasures/membre';
import {ContinentDTO} from '../interface-worldyPleasures/continentDTO';

const URL_API: string = './api/membre';
@Injectable({
  providedIn: 'root'
})
export class MembreService {

  constructor(public http: HttpClient) { }


  query(): Observable<membreList>{
    return this.http.get<membreList>(URL_API);
  }

  get(id:number): Observable<membreDTO>{
    return this.http.get<membreDTO>(URL_API+'/'+id);
  }

  post(membre: membreDTO): Observable<membreDTO> {
    return this.http.post<membreDTO>(URL_API, membre);
  }

  delete(id: number): Observable<any> {
    return this.http.delete(URL_API + '/' + id);
  }

}
