import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ContinentDTO} from '../interface-worldyPleasures/continentDTO';
import {Continent, ContinentList} from '../interface-worldyPleasures/continent';

const URL_API: string = './api/continent';

@Injectable({
  providedIn: 'root'
})
export class ContinentService {

  constructor(public http: HttpClient) { }

  query(): Observable<ContinentList> {
    return this.http.get<ContinentList>(URL_API);
  }

  get(id: number): Observable<ContinentDTO> {
    return this.http.get<ContinentDTO>(URL_API + '/' + id);
  }

  post(continent: ContinentDTO): Observable<ContinentDTO> {
    return this.http.post<ContinentDTO>(URL_API, continent);
  }

  delete(id: number): Observable<any> {
    return this.http.delete(URL_API + '/' + id);
  }

  put(continent: ContinentDTO): Observable<any> {
    return this.http.put(URL_API, continent);
  }

  /*
  getContinents(): Continent[] {
    let continents: Continent[] = [];
    continents.
    this._continents = continents.map(continent => new Continent().fromContinentDTO(continent));
  }
  */

}
