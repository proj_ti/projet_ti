import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ContinentList} from '../interface-worldyPleasures/continent';
import {Pays, PaysList} from '../interface-worldyPleasures/pays';
import {PaysDTO} from '../interface-worldyPleasures/paysDTO';
import {ContinentDTO} from '../interface-worldyPleasures/continentDTO';

const URL_API: string = './api/pays';

@Injectable({
  providedIn: 'root'
})
export class PaysService {

  constructor(public http : HttpClient) { }

  query(): Observable<PaysList> {
    return this.http.get<PaysList>(URL_API);
  }

  get(idContinent : number): Observable<Pays[]> {
    return this.http.get<Pays[]>(URL_API + '/' + idContinent);
  }

  post(pays: PaysDTO): Observable<PaysDTO> {
    return this.http.post<PaysDTO>(URL_API, pays);
  }

  delete(id: number): Observable<any> {
    return this.http.delete(URL_API + '/' + id);
  }

  put(pays: PaysDTO): Observable<any> {
    return this.http.put(URL_API, pays);
  }
}
