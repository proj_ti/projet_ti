import { TestBed } from '@angular/core/testing';

import { Pays2Service } from './pays2.service';

describe('Pays2Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Pays2Service = TestBed.get(Pays2Service);
    expect(service).toBeTruthy();
  });
});
