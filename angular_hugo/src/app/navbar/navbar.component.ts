import {Component, Input, OnInit} from '@angular/core';
import { ListContinentComponent} from '../continent/list-continent/list-continent.component';
import {ContinentDTO} from '../interface-worldyPleasures/continentDTO';
import {MOCK_CONTINENT} from '../mock-worldlyPleasures/mock-continent';
import {Continent, ContinentList} from '../interface-worldyPleasures/continent';
import {Subscription} from 'rxjs';
import {ContinentService} from '../services/continent.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  private _continents: ContinentList = [];
  private subscriptions: Subscription[] = [];

  constructor(public continentService : ContinentService) { }

  ngOnInit() {
    this.loadContinent();
  }

  get continents(): Continent[] {
    return this._continents;
  }


  set continents(value: Continent[]) {
    this._continents = value;
  }

  private loadContinent() {
    const sub = this.continentService
      .query()
      //Possible que ça fonctionne pas
      .subscribe(continents => {
        this._continents = continents.map(continent => new Continent().fromContinentDTO(continent));
        console.log(this._continents);
      });

    this.subscriptions.push(sub);
  }
}

