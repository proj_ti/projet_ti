﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using APIContinent.DTO;
using APIContinent.Model;

namespace APIContinent.DAO
{
    public class paysDAO
    {
        private static readonly string TABLE_NAME = "Pays";

        public static string FIELD_ID = "Id_Pays";
        public static string FIELD_NOM = "Nom_Pays";
        public static string FIELD_IDEX = "Id_Continent";
        
        private static readonly String REQ_QUERY = $"select * from {TABLE_NAME}";

        private static readonly string REQ_POST = $"INSERT INTO {TABLE_NAME} ({FIELD_NOM}, {FIELD_IDEX}) " +
                                                  $"OUTPUT INSERTED.{FIELD_ID} " +
                                                  $"VALUES (@{FIELD_NOM},@{FIELD_IDEX})";

        private static readonly String REQ_DELETE = $"delete from {TABLE_NAME} where {FIELD_ID} = @{FIELD_ID}";

        
        
        public static IEnumerable<paysDTO> QueryPays()
        {
            List<paysDTO> pays = new List<paysDTO>();

            using (SqlConnection connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_QUERY;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    pays.Add(new paysDTO(reader));   
                }
            }
            
            return pays;
        }
        
        public static paysDTO PostPays(paysDTO pays) 
        {
            using (SqlConnection connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_POST;

                command.Parameters.AddWithValue($@"{FIELD_NOM}", pays.Nom_pays);
                command.Parameters.AddWithValue($@"{FIELD_IDEX}", pays.Idex);
               
                
                pays.Id = (int) command.ExecuteScalar();
                
            }    
            return pays;
        }
        
        public static bool DeletePays(int id_pays)
        {
            bool hasBeenDeleted = false;

            using (SqlConnection connection= DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_DELETE;

                command.Parameters.AddWithValue($"@{FIELD_ID}", id_pays);
                hasBeenDeleted = command.ExecuteNonQuery() == 1; 
            }

            return hasBeenDeleted;
        }

    }
}