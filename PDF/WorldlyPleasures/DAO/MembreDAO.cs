﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using APIContinent.DTO;
using APIContinent.Model;

namespace APIContinent.DAO
{
    public class MembreDAO
    {
        private static readonly string TABLE_NAME = "Membre";

        public static string FIELD_ID = "Id_Membre";
        public static string FIELD_NOM = "Nom_Membre";
        public static string FIELD_PRENOM = "Prenom_Membre";
        public static string FIELD_PSEUDO = "Pseudo_Membre";
        public static string FIELD_MAIL = "Mail_Membre";
        public static string FIELD_PASSWORD = "PWD_Membre";
        

        private static readonly String REQ_QUERY = $"select * from {TABLE_NAME}";

        private static readonly string REQ_POST = $"INSERT INTO {TABLE_NAME} ({FIELD_NOM},{FIELD_PRENOM},{FIELD_MAIL},{FIELD_PSEUDO},{FIELD_PASSWORD}) " +
                                                  $" OUTPUT INSERTED.{FIELD_ID}" +
                                                  $" VALUES (@{FIELD_NOM},@{FIELD_PRENOM},@{FIELD_MAIL},@{FIELD_PSEUDO},@{FIELD_PASSWORD})";

       private static readonly String REQ_DELETE = $"delete from {TABLE_NAME} where {FIELD_ID} = @{FIELD_ID}";

      public static MembreDTO PostMembre(MembreDTO membre) 
        {
            using (SqlConnection connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_POST;

                command.Parameters.AddWithValue($@"{FIELD_NOM}", membre.Nom_membre);
                command.Parameters.AddWithValue($@"{FIELD_PRENOM}", membre.Prenom_membre);
                command.Parameters.AddWithValue($@"{FIELD_MAIL}", membre.Mail_membre);
                command.Parameters.AddWithValue($@"{FIELD_PSEUDO}", membre.Pseudo_membre);
                command.Parameters.AddWithValue($@"{FIELD_PASSWORD}", membre.Password_membre);
                membre.Id = (int) command.ExecuteScalar();
                
            }    
            return membre;
        }
      
        public static List<MembreDTO> QueryMembre()
        {
            List<MembreDTO> membre = new List<MembreDTO>();

            using (SqlConnection connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_QUERY;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    membre.Add(new MembreDTO(reader));   
                }
            }
            
            return membre;
        }
        
        public static bool DeleteMembre(int Id_Membre)
        {
            bool hasBeenDeleted = false;

            using (SqlConnection connection= DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_DELETE;

                command.Parameters.AddWithValue($"@{FIELD_ID}", Id_Membre);
                hasBeenDeleted = command.ExecuteNonQuery() == 1; 
            }

            return hasBeenDeleted;
        }
        
    }
}