﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using APIContinent.DTO;
using APIContinent.Model;

namespace APIContinent.DAO
{
    public class photoDAO
    {
        private static readonly string TABLE_NAME = "Photos";

        public static string FIELD_ID = "Id_Photo";
        public static string FIELD_EXE = "Id_Plat";
        public static string FIELD_NOM = "Nom_Photo";
        public static string FIELD_CHEMIN = "Chemin_Photo";
        
        
        private static readonly String REQ_QUERY = $"select * from {TABLE_NAME}";
        private static readonly String REQ_DELETE = $"delete from {TABLE_NAME} where {FIELD_ID} = @{FIELD_ID}";
        
        private static readonly String REQ_POST =
            $"INSERT INTO {TABLE_NAME} ({FIELD_EXE},{FIELD_NOM},{FIELD_CHEMIN}) " +
            $" OUTPUT INSERTED.{FIELD_ID}" +
            $" VALUES (@{FIELD_EXE},@{FIELD_NOM},@{FIELD_CHEMIN})";

        public static List<photoDTO> QueryPhotos()
        {
            List<photoDTO> photos = new List<photoDTO>();

            using (SqlConnection connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_QUERY;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    photos.Add(new photoDTO(reader));   
                }
            }
            
            return photos;
        }
        
        
        public static photoDTO PostPhotos(photoDTO photo) 
        {
            using (SqlConnection connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_POST;

                command.Parameters.AddWithValue($@"{FIELD_EXE}", photo.Idex);
                command.Parameters.AddWithValue($@"{FIELD_NOM}", photo.Nom_photo);
                command.Parameters.AddWithValue($@"{FIELD_CHEMIN}", photo.Chemin_photo);
               
              
                photo.Id = (int) command.ExecuteScalar();
                
            }    
            return photo;
        }
        
        public static bool DeletePhotos(int Id_Photo)
        {
            bool hasBeenDeleted = false;

            using (SqlConnection connection= DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_DELETE;

                command.Parameters.AddWithValue($"@{FIELD_ID}", Id_Photo);
                hasBeenDeleted = command.ExecuteNonQuery() == 1; 
            }

            return hasBeenDeleted;
        }

    }
}