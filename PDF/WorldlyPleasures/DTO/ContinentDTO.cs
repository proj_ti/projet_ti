﻿using System;
using System.Data.SqlClient;
using APIContinent.DAO;

namespace APIContinent.DTO
{
    public class continentDTO
    {
       public int Id { get; set; }

       public string Nom_continent { get; set; }

       public continentDTO(int id_continent, string nom_continent)
       {
           Id = id_continent;
           Nom_continent = nom_continent;
       }

       public continentDTO()
       {
       }

       public continentDTO(SqlDataReader reader)
       {
           Id = Convert.ToInt32(reader[ContinentDAO.FIELD_ID].ToString());
           Nom_continent = reader[ContinentDAO.FIELD_NOM].ToString();

       }
    }
}