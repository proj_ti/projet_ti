﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using APIContinent.DAO;
using APIContinent.DTO;

namespace APIContinent.Controllers
{
    [ApiController]
        [Route("api/[controller]")]
        public class  ContinentController : ControllerBase
        {
            [HttpGet]
            public IEnumerable<continentDTO> Get()
            {
                return ContinentDAO.QueryContinent();
            }
            
            [HttpPost]
            public continentDTO Post([FromBody]continentDTO continentDto)
            {
                return ContinentDAO.POSTContinent(continentDto);
            }
            //coucou
            [HttpDelete("{id}")]
            public ActionResult Delete(int id)
            {
                if (ContinentDAO.DeleteContinent(id))
                {
                    return Ok();
                }
                return BadRequest();
            }
        }
    }
