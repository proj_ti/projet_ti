﻿using System.Collections.Generic;
using APIContinent.DAO;
using APIContinent.DTO;
using Microsoft.AspNetCore.Mvc;

namespace APIContinent.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AdminController : ControllerBase
    {
        [HttpGet]
        public IEnumerable<AdminDTO> Get()
        {
            return AdminDAO.QueryAdmin();
        }

        [HttpPost]
        public AdminDTO Post([FromBody] AdminDTO admin)
        {
            return AdminDAO.PostAdmin(admin);
        }
    }
}