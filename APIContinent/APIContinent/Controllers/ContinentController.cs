﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using APIContinent.DAO;
using APIContinent.DTO;

namespace APIContinent.Controllers
{
    [ApiController]
        [Route("api/[controller]")]
        public class  ContinentController : ControllerBase
        {
            [HttpGet]
            public IEnumerable<continentDTO> Get()
            {
                return ContinentDAO.Query();
            }
            
            [HttpPost]
            public continentDTO Post([FromBody]continentDTO continentDto)
            {
                return ContinentDAO.Post(continentDto);
            }
            
            [HttpDelete("{id}")]
            public ActionResult Delete(int id)
            {
                if (ContinentDAO.Delete(id))
                {
                    return Ok();
                }
                return BadRequest();
            }
        }
    }
