﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using APIContinent.DTO;
using APIContinent.Model;

namespace APIContinent.DAO
{
    public class ContinentDAO
    {
        private static readonly string TABLE_NAME = "continent";

        public static string FIELD_ID = "id_continent";
        public static string FIELD_NOM = "nom_continent";

        private static readonly String REQ_QUERY = $"select * from {TABLE_NAME}";

        private static readonly string REQ_POST = $"INSERT INTO {TABLE_NAME} ({FIELD_NOM}) " +
                                                  $"OUTPUT INSERTED.{FIELD_ID} " +
                                                  $"VALUES (@{FIELD_NOM})";

        private static readonly String REQ_DELETE = $"delete from {TABLE_NAME} where {FIELD_ID} = @{FIELD_ID}";

        public static continentDTO Post(continentDTO continent) 
        {
            using (SqlConnection connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_POST;

                command.Parameters.AddWithValue($@"{FIELD_NOM}", continent.Nom_continent);
               
                
                continent.Id = (int) command.ExecuteScalar();
                
            }    
            return continent;
        }
        
        public static List<continentDTO> Query()
        {
            List<continentDTO> continent = new List<continentDTO>();

            using (SqlConnection connection = DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_QUERY;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    continent.Add(new continentDTO(reader));   
                }
            }
            
            return continent;
        }
        
        public static bool Delete(int id_continent)
        {
            bool hasBeenDeleted = false;

            using (SqlConnection connection= DataBase.GetConnection())
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = REQ_DELETE;

                command.Parameters.AddWithValue($"@{FIELD_ID}", id_continent);
                hasBeenDeleted = command.ExecuteNonQuery() == 1; 
            }

            return hasBeenDeleted;
        }
        
    }
}