import { Component } from '@angular/core';
import {Continent} from './interface-worldyPleasures/Continent';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'worldlyPleasures';
  private continents : Continent[] = [];

  constructor() {}
  nfOnInit() {
  }

}
