import { Component, OnInit } from '@angular/core';
import {Continent} from '../../interface-worldyPleasures/Continent';
import {MOCK_CONTINENT} from '../../mock-worldlyPleasures/mock-continent';

@Component({
  selector: 'app-list-continent',
  templateUrl: './list-continent.component.html',
  styleUrls: ['./list-continent.component.css']
})
export class ListContinentComponent implements OnInit {

  private _continents: Continent[] = MOCK_CONTINENT;

  constructor() { }

  ngOnInit() {
  }

  get continents() : Continent[]{
    return this._continents;
  }

  set continents(value : Continent[]){
    this._continents = value;
  }
}
