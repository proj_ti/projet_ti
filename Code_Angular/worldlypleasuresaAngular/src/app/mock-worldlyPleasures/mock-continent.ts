import {Continent} from '../interface-worldyPleasures/Continent';

export const MOCK_CONTINENT: Continent[] = [
  {
    idContinent: 1,
    nomContinent: 'Afrique'
  },
  {
    idContinent: 2,
    nomContinent: 'Europe'
  },
  {
    idContinent: 3,
    nomContinent: 'Amerique'
  },
  {
    idContinent: 4,
    nomContinent: 'Asie'
  }
];

