import { Component, OnInit } from '@angular/core';
import { ListContinentComponent} from '../continent/list-continent/list-continent.component';
import {Continent} from '../interface-worldyPleasures/Continent';
import {MOCK_CONTINENT} from '../mock-worldlyPleasures/mock-continent';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  continents: Continent[] = MOCK_CONTINENT;

  constructor() { }

  ngOnInit() {
  }

}
