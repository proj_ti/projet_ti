import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {appRoutingModule} from './app.routing';

// @ts-ignore
import { AppComponent } from './app.component';
import { ListContinentComponent } from './continent/list-continent/list-continent.component';
import { NavbarComponent } from './navbar/navbar.component';
import { IndexComponent } from './Pages/Index/index.component';
import { SectionTitreMenuComponent } from './Pages/Index/section-titre-menu/section-titre-menu.component';
import { ContinentComponent } from './Pages/continent/continent.component';
import { PaysComponent } from './Pages/pays/pays.component';
import { PlaylistComponent } from './Pages/playlist/playlist.component';
import { ConnexionComponent } from './Pages/connexion/connexion.component';
import { InscriptionComponent } from './Pages/inscription/inscription.component';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    ListContinentComponent,
    NavbarComponent,
    IndexComponent,
    SectionTitreMenuComponent,
    ContinentComponent,
    PaysComponent,
    PlaylistComponent,
    ConnexionComponent,
    InscriptionComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    appRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
