export declare type  ContinentList = Continent[];

export interface Continent {
  idContinent: number;
  nomContinent: string;
}
