import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionPresentationPlatComponent } from './section-presentation-plat.component';

describe('SectionPresentationPlatComponent', () => {
  let component: SectionPresentationPlatComponent;
  let fixture: ComponentFixture<SectionPresentationPlatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SectionPresentationPlatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionPresentationPlatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
