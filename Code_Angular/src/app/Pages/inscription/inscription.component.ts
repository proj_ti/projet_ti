import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css']
})
export class InscriptionComponent implements OnInit {

  form : FormGroup = this.fb.group( {
    prenomInscription : this.fb.control('', Validators.required),
    nomInscription : this.fb.control('', Validators.required),
    mailInscription : this.fb.control('', [Validators.required, Validators.email]),
    motDePasseInscription : this.fb.control('', [Validators.required, Validators.minLength(8.)])
  });
  submitted = false;

  constructor(public fb : FormBuilder) { }

  ngOnInit() {
  }

  get f() { return this.form.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.form.invalid) {
      return;
    }
  }

}
