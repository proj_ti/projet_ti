import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionPresentationContinentComponent } from './section-presentation-continent.component';

describe('SectionPresentationContinentComponent', () => {
  let component: SectionPresentationContinentComponent;
  let fixture: ComponentFixture<SectionPresentationContinentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SectionPresentationContinentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionPresentationContinentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
