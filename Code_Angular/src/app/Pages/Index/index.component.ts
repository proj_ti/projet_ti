import { Component, OnInit } from '@angular/core';
import {Continent} from '../../interface-worldyPleasures/continent';
import {ContinentService} from '../../services/continent.service';
import {Subscription} from 'rxjs';


@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  private _continents: Continent[] = [];
  private subscriptions: Subscription[] = [];

  constructor(public continentService : ContinentService) { }

  ngOnInit() {
    this.loadContinent();
  }

  private loadContinent() {
    const sub = this.continentService
      .query()
      //Possible que ça fonctionne pas
      .subscribe(continents =>
        this._continents = this.continents.map(continent => new Continent().fromArticleDTO(continent))
      );

    this.subscriptions.push(sub);
  }

  get continents(): Continent[] {
    return this._continents;
  }

  set continents(value: Continent[]) {
    this._continents = value;
  }
}
