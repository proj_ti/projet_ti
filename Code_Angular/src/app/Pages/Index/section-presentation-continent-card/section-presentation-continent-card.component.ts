import { Component, OnInit } from '@angular/core';
import {ListContinentComponent} from '../../../continent/list-continent/list-continent.component';
import {MOCK_CONTINENT} from '../../../mock-worldlyPleasures/mock-continent';
import {ContinentDTO} from '../../../interface-worldyPleasures/continentDTO';

@Component({
  selector: 'app-section-presentation-continent-card',
  templateUrl: './section-presentation-continent-card.component.html',
  styleUrls: ['./section-presentation-continent-card.component.css']
})
export class SectionPresentationContinentCardComponent implements OnInit {

  continents : ContinentDTO[] = MOCK_CONTINENT;
  constructor() { }

  ngOnInit() {
  }

}
