import { Component, OnInit } from '@angular/core';
import { ListContinentComponent} from '../continent/list-continent/list-continent.component';
import {ContinentDTO} from '../interface-worldyPleasures/continentDTO';
import {MOCK_CONTINENT} from '../mock-worldlyPleasures/mock-continent';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  continents: ContinentDTO[] = MOCK_CONTINENT;

  constructor() { }

  ngOnInit() {
  }

}
