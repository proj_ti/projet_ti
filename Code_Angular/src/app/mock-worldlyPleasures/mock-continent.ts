import {ContinentDTO} from '../interface-worldyPleasures/continentDTO';

export const MOCK_CONTINENT: ContinentDTO[] = [
  {
    idContinent: 1,
    nomContinent: 'Afrique'
  },
  {
    idContinent: 2,
    nomContinent: 'Europe'
  },
  {
    idContinent: 3,
    nomContinent: 'Amerique'
  },
  {
    idContinent: 4,
    nomContinent: 'Asie'
  }
];

