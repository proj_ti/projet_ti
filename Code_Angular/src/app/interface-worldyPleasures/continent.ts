import {ContinentDTO} from './continentDTO';

export declare type  ContinentList = ContinentDTO[];

export class Continent{
  private _id:number;
  private _nom:string;

  constructor(id: number = -1, nom: string = ''){
    this._id = id;
    this._nom = nom;
  }

  toContinentDTO(): ContinentDTO{
    return {
      idContinent: this._id,
      nomContinent: this._nom
    };
  }

  fromArticleDTO(dto: Continent): Continent {
    Object.assign(this, dto);
    return this;
  }

  get id(): number{
    return this._id;
  }

  get nom(): string{
    return this._nom;
  }

  set id(value: number){
    this._id = value;
  }

  set nom(value: string){
    this._nom = value;
  }

  equals(obj): boolean {
    if (obj instanceof Continent) {
      return this._id === (<Continent>obj)._id;
    }
    return false;
  }

}

