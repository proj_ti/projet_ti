export interface ContinentDTO {
  idContinent: number;
  nomContinent: string;
}
