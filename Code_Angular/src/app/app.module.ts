import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {appRoutingModule} from './app.routing';

import { AppComponent } from './app.component';
import { ListContinentComponent } from './continent/list-continent/list-continent.component';
import { NavbarComponent } from './navbar/navbar.component';
import { IndexComponent } from './Pages/Index/index.component';
import { SectionTitreMenuComponent } from './Pages/Index/section-titre-menu/section-titre-menu.component';
import { ContinentComponent } from './Pages/continent/continent.component';
import { PaysComponent } from './Pages/pays/pays.component';
import { PlaylistComponent } from './Pages/playlist/playlist.component';
import { ConnexionComponent } from './Pages/connexion/connexion.component';
import { InscriptionComponent } from './Pages/inscription/inscription.component';
import {ReactiveFormsModule} from '@angular/forms';
import { SectionPresentationSiteComponent } from './Pages/Index/section-presentation-site/section-presentation-site.component';
import { SectionPresentationContinentCardComponent } from './Pages/Index/section-presentation-continent-card/section-presentation-continent-card.component';
import { SectionPresentationContinentComponent } from './Pages/continent/section-presentation-continent/section-presentation-continent.component';
import { SectionTitreContinentComponent } from './Pages/continent/section-titre-continent/section-titre-continent.component';
import { SectionTitrePaysCardComponent } from './Pages/continent/section-titre-pays-card/section-titre-pays-card.component';
import { SectionTitrePaysComponent } from './Pages/pays/section-titre-pays/section-titre-pays.component';
import { SectionPresentationPaysComponent } from './Pages/pays/section-presentation-pays/section-presentation-pays.component';
import { SectionPresentationPlatComponent } from './Pages/pays/section-presentation-plat/section-presentation-plat.component';

@NgModule({
  declarations: [
    AppComponent,
    ListContinentComponent,
    NavbarComponent,
    IndexComponent,
    SectionTitreMenuComponent,
    ContinentComponent,
    PaysComponent,
    PlaylistComponent,
    ConnexionComponent,
    InscriptionComponent,
    SectionPresentationSiteComponent,
    SectionPresentationContinentCardComponent,
    SectionPresentationContinentComponent,
    SectionTitreContinentComponent,
    SectionTitrePaysCardComponent,
    SectionTitrePaysComponent,
    SectionPresentationPaysComponent,
    SectionPresentationPlatComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    appRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
