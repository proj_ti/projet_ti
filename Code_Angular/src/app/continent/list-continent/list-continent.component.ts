import { Component, OnInit } from '@angular/core';
import {ContinentDTO} from '../../interface-worldyPleasures/continentDTO';
import {MOCK_CONTINENT} from '../../mock-worldlyPleasures/mock-continent';

@Component({
  selector: 'app-list-continent',
  templateUrl: './list-continent.component.html',
  styleUrls: ['./list-continent.component.css']
})
export class ListContinentComponent implements OnInit {

  private _continents: ContinentDTO[] = MOCK_CONTINENT;

  constructor() { }

  ngOnInit() {
  }

  get continents() : ContinentDTO[]{
    return this._continents;
  }

  set continents(value : ContinentDTO[]){
    this._continents = value;
  }
}
